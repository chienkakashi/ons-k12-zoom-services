<?php

namespace App\Console\Commands;

use App\Services\ZoomService;
use Illuminate\Console\Command;

class CreateMeeting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zoom:create-meetings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new meetings';

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $scheduled_meetings = ZoomService::getScheduledMeetings();

        if (!$scheduled_meetings) exit();

        foreach ($scheduled_meetings as $key => $meeting) {
            if ((time() >= $meeting['start_time'] && time() <= $meeting['end_time']) && $meeting['zoom_meeting_id'] == '') {
                $new_meeting = ZoomService::createMeeting($meeting['topic'], $meeting['duration'], $meeting['approval_type']);
                if ($new_meeting['data']) {
                    $scheduled_meetings[$key]['zoom_meeting_id']        = $new_meeting['data']['id'];
                    $scheduled_meetings[$key]['start_url']              = $new_meeting['data']['start_url'];
                    $scheduled_meetings[$key]['join_url']               = $new_meeting['data']['join_url'];
                    $scheduled_meetings[$key]['host']                   = $new_meeting['data']['host'];
                    $scheduled_meetings[$key]['zoom_meeting_password']  = $new_meeting['data']['password'];

                    ZoomService::setScheduledMeetings($scheduled_meetings);
                }
            }
        }
    }
}