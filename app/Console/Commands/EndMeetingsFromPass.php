<?php

namespace App\Console\Commands;

use App\Services\ZoomService;
use Illuminate\Console\Command;

class EndMeetingsFromPass extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zoom:end-meetings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'End meetings from the pass';

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $scheduled_meetings = ZoomService::getScheduledMeetings();
        $available_links    = ZoomService::getAvailableLinks();

        if (!$scheduled_meetings) exit();

        foreach ($scheduled_meetings as $key => $meeting) {
            if (time() >= $meeting['end_time']) {
                if ($meeting['zoom_meeting_id']) {
                    $detailMeeting = ZoomService::getDetailMeeting($meeting['zoom_meeting_id']);
                    if (isset($detailMeeting['data']['status'])) {
                        switch ($detailMeeting['data']['status']) {
                            case 'waiting':
                                ZoomService::deleteMeeting($meeting['zoom_meeting_id']);
                                break;
                            case 'started':
                                ZoomService::endMeeting($meeting['zoom_meeting_id']);
                                break;
                        }
                    }

                    unset($available_links[$meeting['zoom_meeting_id']]);
                }

                unset($scheduled_meetings[$key]);
            }
        }

        ZoomService::setScheduledMeetings($scheduled_meetings);
        ZoomService::setAvailableLinks($available_links);
    }
}