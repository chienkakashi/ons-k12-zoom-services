<?php

namespace App\Services;

use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;
use stdClass;
use Illuminate\Support\Facades\Log;

class ZoomService
{

    public static function getSignature($meeting_id, $role): string {
        // Create token header as a JSON string
        $header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);
        $iat = time();

        // Create token payload as a JSON string
        $payload = json_encode([
            'sdkKey' => env("ZOOM_MEETING_SDK_KEY"),
            'mn' => $meeting_id,
            'role' => $role,
            'iat' => $iat,
            'exp' => $iat + 60 * 60 * 2,
            'appKey' => env('ZOOM_MEETING_SDK_KEY'),
            'tokenExp' => $iat + 60 * 60 * 2
        ]);

        // Encode Header to Base64Url String
        $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));

        // Encode Payload to Base64Url String
        $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));

        // Create Signature Hash
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, env("ZOOM_MEETING_SDK_SECRET"), true);

        // Encode Signature to Base64Url String
        $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

        // Create JWT
        $jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;

        return $jwt;
    }

    public static function getZAKToken($host) {
        $client  = new Client(['base_uri' => 'https://api.zoom.us']);
        $message = 'success';
        $code    = 200;
        $data    = [];

        ZoomService::resetToken();
        $token = ZoomService::getToken();

        try {
            // Gọi API tạo meeting
            $response = $client->request('GET', "/v2/users/{$host}/token?type=zak", [
                "headers" => [
                    "Authorization" => "Bearer $token->access_token"
                ]
            ]);

            $data = json_decode($response->getBody(), true);
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            $code    = $e->getCode();
        }

        return compact('message', 'code', 'data');

    }

    public static function createMeeting($topic, $duration, $approval_type)
    {
        // Log::info("Start creating meeting", compact('topic', 'duration'));
        $client  = new Client(['base_uri' => 'https://api.zoom.us']);
        $message = 'success';
        $code    = 200;
        $data    = [];

        // Lấy danh sách available hosts
        $hosts = ZoomService::getAvailableHosts();
        if (!$hosts) {
            $message = 'Bạn đã đạt đến số lượng cuộc họp tối đa diễn ra trong cùng một thời điểm.';

            return compact('message', 'code', 'data');
        }

        // Lấy host đầu tiên trong list available hosts
        $host = reset($hosts);

        ZoomService::resetToken();
        $token = ZoomService::getToken();

        try {
            $settings = [
                'approval_type'                  => $approval_type,
                'registration_type'              => 1,
                'registrants_confirmation_email' => false,
                'auto_recording'                 => 'cloud',
                'join_before_host'               => true,
                // 'waiting_room'                   => false,
            ];
            // Gọi API tạo meeting
            $response = $client->request('POST', "/v2/users/{$host}/meetings", [
                "headers" => [
                    "Authorization" => "Bearer $token->access_token"
                ],
                'json'    => [
                    "topic"    => urldecode($topic),
                    "type"     => 2,
                    "duration" => $duration,
                    "default_password" => false,
                    "settings" => $settings
                ],
            ]);

            $data = json_decode($response->getBody(), true);
            $data['host'] = $host;
            // Log::info("Create meeting successfully", $data);

            // Remove unavailable host from list
            ZoomService::removeUnavailableHost($host);
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            $code    = $e->getCode();

            // Log::error($e->getMessage(), compact('topic', 'duration'));
        }

        return compact('message', 'code', 'data');
    }

    public static function getAvailableHosts()
    {
        return json_decode(Storage::disk('local')->get('available_hosts.txt'), true);
    }

    public static function removeUnavailableHost($unavailableHost)
    {
        $hosts = ZoomService::getAvailableHosts();
        if (($key = array_search($unavailableHost, $hosts)) !== false) {
            unset($hosts[$key]);
        }

        ZoomService::setAvailableHosts($hosts);
    }

    public static function setAvailableHosts($hosts)
    {
        Storage::disk('local')->put('available_hosts.txt', json_encode($hosts));
    }

    public static function getDetailMeeting($zoom_meeting_id)
    {
        $client  = new Client(['base_uri' => 'https://api.zoom.us']);
        $message = 'success';
        $code    = 200;
        $data    = [];

        ZoomService::resetToken();
        $token = ZoomService::getToken();

        try {
            $response = $client->request('GET', "/v2/meetings/{$zoom_meeting_id}", [
                "headers" => [
                    "Authorization" => "Bearer $token->access_token"
                ]
            ]);

            $data = json_decode($response->getBody(), true);
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            $code    = $e->getCode();
        }

        return compact('message', 'code', 'data');
    }

    public static function endMeeting($zoom_meeting_id)
    {
        $client  = new Client(['base_uri' => 'https://api.zoom.us']);
        $message = 'success';
        $code    = 200;
        $data    = [];

        ZoomService::resetToken();
        $token = ZoomService::getToken();

        try {
            $response = $client->request("PUT", "/v2/meetings/{$zoom_meeting_id}/status", [
                "headers" => [
                    "Authorization" => "Bearer $token->access_token"
                ],
                'json'    => [
                    "action" => "end",
                ],
            ]);

        }
        catch (Exception $e) {
            $message = $e->getMessage();
            $code    = $e->getCode();
            // Log::error($e->getMessage(), compact('zoom_meeting_id'));
        }

        return compact('message', 'code', 'data');
    }

    public static function deleteMeeting($zoom_meeting_id)
    {
        $client  = new Client(['base_uri' => 'https://api.zoom.us']);
        $message = 'success';
        $code    = 200;
        $data    = [];

        ZoomService::resetToken();
        $token = ZoomService::getToken();

        try {
            $response = $client->request("DELETE", "/v2/meetings/{$zoom_meeting_id}", [
                "headers" => [
                    "Authorization" => "Bearer $token->access_token"
                ]
            ]);
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            $code    = $e->getCode();
        }

        return compact('message', 'code', 'data');
    }

    public static function canCreateScheduledMeeting($start_time, $end_time, $excepted_meeting_ids = [])
    {
        $scheduled_meetings = ZoomService::getScheduledMeetings();
        if (!$scheduled_meetings) return true;

        $hosts                  = ZoomService::getHosts();
        $count_existed_meetings = 0;
        foreach ($scheduled_meetings as $meeting) {
            if (in_array($meeting['meeting_id'], $excepted_meeting_ids))
                continue;

            if ($start_time <= $meeting['end_time'] && $end_time >= $meeting['start_time'])
                $count_existed_meetings++;
        }

        return $count_existed_meetings < count($hosts);
    }

    public static function getScheduledMeetings()
    {
        return json_decode(Storage::disk('local')->get('scheduled_meetings.txt'), true);
    }

    public static function getHosts()
    {
        $hosts = json_decode(Storage::disk('local')->get('hosts.txt'), true);
        asort($hosts);
        return $hosts;
    }

    public static function setHosts($hosts)
    {
        Storage::disk('local')->put('hosts.txt', json_encode($hosts));
    }

    public static function setScheduledMeetings($scheduled_meetings)
    {
        try {
            Storage::disk('local')->put('scheduled_meetings.txt', json_encode($scheduled_meetings));
            // Log::info('setScheduledMeetings successfully', $scheduled_meetings);

            return true;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit(1);
            // Log::error($e->getMessage(), $scheduled_meetings);
        }

        return false;
    }

    public function createMeetingInviteLink($meeting_id, $user_id, $first_name, $last_name): array
    {
        $client  = new Client(['base_uri' => 'https://api.zoom.us']);
        $message = 'success';
        $code    = 200;
        $data    = new stdClass();

        $available_links = ZoomService::getAvailableLinks();
        if (isset($available_links[$meeting_id][$user_id])) {
            $data->join_url = $available_links[$meeting_id][$user_id];

            return compact('message', 'code', 'data');
        }

        ZoomService::resetToken();
        $token = ZoomService::getToken();

        try {
            $response = $client->request('POST', "/v2/meetings/{$meeting_id}/invite_links", [
                "headers" => [
                    "Authorization" => "Bearer $token->access_token"
                ],
                'json'    => [
                    "attendees" => [
                        ["name" => urldecode($first_name) . " " . urldecode($last_name)]
                    ],
                    "ttl"      => 1000,
                ],
            ]);

            $response = json_decode($response->getBody());
            $data->join_url = $response->attendees[0]->join_url;

            $available_links[$meeting_id][$user_id] = $response->attendees[0]->join_url;
            ZoomService::setAvailableLinks($available_links);

        }
        catch (Exception $e) {
            $message = $e->getMessage();
            $code    = $e->getCode();
            // Log::error($e->getMessage(), compact('meeting_id', 'user_id', 'first_name', 'last_name'));
        }

        return compact('message', 'code', 'data');
    }

    public function addMeetingRegistrant($meeting_id, $user_id, $first_name, $last_name): array
    {
        $client  = new Client(['base_uri' => 'https://api.zoom.us']);
        $email   = bin2hex(random_bytes(20)) . '@onschool.edu.vn';
        $message = 'success';
        $code    = 200;
        $data    = new stdClass();

        $available_links = ZoomService::getAvailableLinks();
        if (isset($available_links[$meeting_id][$user_id])) {
            $data->join_url = $available_links[$meeting_id][$user_id];

            return compact('message', 'code', 'data');
        }

        ZoomService::resetToken();
        $token = ZoomService::getToken();

        try {
            $response = $client->request('POST', "/v2/meetings/{$meeting_id}/registrants", [
                "headers" => [
                    "Authorization" => "Bearer $token->access_token"
                ],
                'json'    => [
                    "first_name" => urldecode($first_name),
                    "last_name"  => urldecode($last_name),
                    "email"      => $email,
                ],
            ]);

            $response = json_decode($response->getBody());
            $data->join_url = $response->join_url;

            $available_links[$meeting_id][$user_id] = $response->join_url;
            ZoomService::setAvailableLinks($available_links);

        }
        catch (Exception $e) {
            $message = $e->getMessage();
            $code    = $e->getCode();
            // Log::error($e->getMessage(), compact('meeting_id', 'user_id', 'first_name', 'last_name'));
        }

        return compact('message', 'code', 'data');
    }

    public static function getAvailableLinks()
    {
        return json_decode(Storage::disk('local')->get('available_links.txt'), true);
    }

    public static function resetToken()
    {
        $token = json_decode(Storage::disk('local')->get('zoom_oauth.txt'));

        // Lấy refresh_token
        $refresh_token = $token->refresh_token;

        $client = new Client(['base_uri' => 'https://zoom.us']);
        // Thực hiện gọi API oauth với refresh_token được truyền vào để lấy lại access_token
        $response = $client->request('POST', '/oauth/token', [
            "headers"     => [
                "Authorization" => "Basic " . base64_encode(env("ZOOM_CLIENT_ID") . ':' . env("ZOOM_CLIENT_SECRET"))
            ],
            'form_params' => [
                "grant_type"    => "refresh_token",
                "refresh_token" => $refresh_token
            ],
        ]);

        Storage::disk('local')->put('zoom_oauth.txt', $response->getBody());
    }

    public static function getToken()
    {
        return json_decode(Storage::disk('local')->get('zoom_oauth.txt'));
    }

    public static function setAvailableLinks($available_links)
    {
        Storage::disk('local')->put('available_links.txt', json_encode($available_links));
    }

    public function getUsers()
    {
        $client  = new Client(['base_uri' => 'https://api.zoom.us']);
        $message = 'success';
        $code    = 200;
        $data    = [];

        ZoomService::resetToken();
        $token = ZoomService::getToken();

        try {
            // Gọi API tạo meeting
            $response = $client->request('GET', '/v2/users?page_size=300', [
                "headers" => [
                    "Authorization" => "Bearer $token->access_token"
                ]
            ]);

            $data = json_decode($response->getBody(), true);
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            $code    = $e->getCode();
        }

        return compact('message', 'code', 'data');
    }
}
