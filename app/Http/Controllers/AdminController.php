<?php

namespace App\Http\Controllers;

use App\Services\ZoomService;

class AdminController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function index() {
        $scheduled_meetings = (array) ZoomService::getScheduledMeetings();
        $not_start_meetings = [];

        foreach ($scheduled_meetings as $key => $meeting) {
            $not_start_meetings[] = [
                'meeting_id'    => $meeting['meeting_id'],
                'topic'         => urldecode($meeting['topic']),
                'start_time'    => date('d/m/Y H:i:s', $meeting['start_time']),
                'duration'      => $meeting['duration'],
                'start_url'     => $meeting['start_url'],
                'join_url'      => $meeting['join_url'],
                'approval_type' => $meeting['approval_type'],
            ];
        }

        return view('admin.config_meeting', ['not_start_meetings' => $not_start_meetings]);
    }
}