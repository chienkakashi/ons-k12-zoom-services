<?php

namespace App\Http\Controllers;

use App\Services\ZoomService;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class ZoomController extends Controller
{
    private $service;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->service = new ZoomService();
    }

    public function getClientInfo($meeting_id, $user_id, $password, $first_name, $last_name) {
        $scheduled_meetings = ZoomService::getScheduledMeetings();
        if (!isset($scheduled_meetings[$meeting_id])) {
            // Log::info('ERROR joining meeting: meeting_id not found', compact('meeting_id', 'user_id', 'password', 'first_name', 'last_name'));
            return response()->json(['code' => 400, 'message' => 'Không tìm thấy buổi học. Vui lòng thử lại sau.', 'data' => []]);
        }

        if (!in_array($password, [$scheduled_meetings[$meeting_id]['participant_password'], $scheduled_meetings[$meeting_id]['host_password']])) {
            // Log::info('ERROR joining meeting: password incorrect', compact('meeting_id', 'user_id', 'password', 'first_name', 'last_name'));
            return response()->json(['code' => 400, 'message' => 'Mật khẩu không chính xác. Vui lòng thử lại sau.', 'data' => []]);
        }

        $countrequest = 0;
        while ($scheduled_meetings[$meeting_id]['zoom_meeting_id'] == '' && $countrequest <= 180) {
            sleep(1);
            $scheduled_meetings = ZoomService::getScheduledMeetings();
            $countrequest++;
        }

        if (!$scheduled_meetings[$meeting_id]['zoom_meeting_id'])
            return response()->json(['code' => 400, 'message' => time(). ' Hệ thống đang trong quá trình thiết lập buổi học. Vui lòng thử lại sau hoặc liên hệ với admin để được hỗ trợ.', 'data' => []]);


        switch ($password) {
            case $scheduled_meetings[$meeting_id]['participant_password']:
                $signature = ZoomService::getSignature($scheduled_meetings[$meeting_id]['zoom_meeting_id'], 0);
                return response()->json(['code' => 200, 'message' => 'Success', 'data' => [
                    'signature' => $signature,
                    'sdk_key' => env('ZOOM_MEETING_SDK_KEY'),
                    'meeting_number' => $scheduled_meetings[$meeting_id]['zoom_meeting_id'],
                    'password' => $scheduled_meetings[$meeting_id]['zoom_meeting_password'],
                    'username' => urldecode($first_name . ' ' . $last_name),
                    'user_email' => '',
                    'registrant_token' => '',
                    'zak_token' => '',
                    'leave_url' => env('ZOOM_PARTICIPANT_LEAVE_URL')
                ]]);
            case $scheduled_meetings[$meeting_id]['host_password']:
                $signature = ZoomService::getSignature($scheduled_meetings[$meeting_id]['zoom_meeting_id'], 1);
                $zak_token = ZoomService::getZAKToken($scheduled_meetings[$meeting_id]['host']);

                if($zak_token['code'] != 200)
                    return response()->json(['code' => $zak_token['code'], 'message' => $zak_token['message']]);
                
                $zak_token = $zak_token['data']['token'];

                return response()->json(['code' => 200, 'message' => 'Success', 'data' => [
                    'signature' => $signature,
                    'sdk_key' => env('ZOOM_MEETING_SDK_KEY'),
                    'meeting_number' => $scheduled_meetings[$meeting_id]['zoom_meeting_id'],
                    'password' => $scheduled_meetings[$meeting_id]['zoom_meeting_password'],
                    'username' => urldecode($first_name . ' ' . $last_name),
                    'user_email' => '',
                    'registrant_token' => '',
                    'zak_token' => $zak_token,
                    'leave_url' => env('ZOOM_HOST_LEAVE_URL')
                ]]);
        }

        return response()->json(['code' => 500, 'message' => 'Có vấn đề xảy ra. Vui lòng thử lại sau hoặc liên hệ với admin để được giải quyết.', 'data' => []]);
    }

    public function join($meeting_id, $user_id, $password, $first_name, $last_name) {
        $scheduled_meetings = ZoomService::getScheduledMeetings();
        if (!isset($scheduled_meetings[$meeting_id])) {
            exit('Không tìm thấy buổi học. Vui lòng thử lại sau.');
        }

        if($scheduled_meetings[$meeting_id]['approval_type'] == 0) {
            return view('waiting_zoom', compact('meeting_id', 'user_id', 'password', 'first_name', 'last_name'));
        }

        switch ($password) {
            case $scheduled_meetings[$meeting_id]['participant_password']:
                // return view('client_view', compact('meeting_id', 'user_id', 'password', 'first_name', 'last_name'));
            case $scheduled_meetings[$meeting_id]['host_password']:
                return view('waiting_zoom', compact('meeting_id', 'user_id', 'password', 'first_name', 'last_name'));
            default: exit('Mật khẩu không chính xác. Vui lòng thử lại sau.');
        }
    }

    public function doJoinMeeting($meeting_id, $user_id, $password, $first_name, $last_name)
    {
        // Log::info('Start joining meeting', compact('meeting_id', 'user_id', 'password', 'first_name', 'last_name'));
        $scheduled_meetings = ZoomService::getScheduledMeetings();
        if (!isset($scheduled_meetings[$meeting_id])) {
            // Log::info('ERROR joining meeting: meeting_id not found', compact('meeting_id', 'user_id', 'password', 'first_name', 'last_name'));
            return response()->json(['code' => 400, 'message' => 'Không tìm thấy buổi học. Vui lòng thử lại sau.', 'data' => []]);
        }

        if (!in_array($password, [$scheduled_meetings[$meeting_id]['participant_password'], $scheduled_meetings[$meeting_id]['host_password']])) {
            // Log::info('ERROR joining meeting: password incorrect', compact('meeting_id', 'user_id', 'password', 'first_name', 'last_name'));
            return response()->json(['code' => 400, 'message' => 'Mật khẩu không chính xác. Vui lòng thử lại sau.', 'data' => []]);
        }

        $countrequest = 0;
        while ($scheduled_meetings[$meeting_id]['zoom_meeting_id'] == '' && $countrequest <= 180) {
            sleep(1);
            $scheduled_meetings = ZoomService::getScheduledMeetings();
            $countrequest++;
        }

        if (!$scheduled_meetings[$meeting_id]['zoom_meeting_id'])
            return response()->json(['code' => 400, 'message' => time(). ' Hệ thống đang trong quá trình thiết lập buổi học. Vui lòng thử lại sau hoặc liên hệ với admin để được hỗ trợ.', 'data' => []]);

        switch ($password) {
            case $scheduled_meetings[$meeting_id]['participant_password']:
                $response = $this->joinWithCustomName($scheduled_meetings[$meeting_id], $user_id, $first_name, $last_name);
                $response = $response->getData(true);
                
                $join_url = $response['join_url'] ?? '';
                // Log::info('Join meeting successfully', $response);

                return response()->json(['code' => 200, 'message' => 'Success', 'data' => ['link' => $join_url]]);
            case $scheduled_meetings[$meeting_id]['host_password']:
                // Log::info('Join meeting successfully', $scheduled_meetings[$meeting_id]);
                return response()->json(['code' => 200, 'message' => 'Success', 'data' => ['link' => $scheduled_meetings[$meeting_id]['start_url']]]);
        }

        return response()->json(['code' => 500, 'message' => 'Có vấn đề xảy ra. Vui lòng thử lại sau hoặc liên hệ với admin để được giải quyết.', 'data' => []]);
    }

    public function joinWithCustomName($scheduled_meeting, $user_id, $first_name, $last_name)
    {
        try {
            $join_url = '';

            if($scheduled_meeting['approval_type'] == 2) {
                $response = $this->service->createMeetingInviteLink($scheduled_meeting['zoom_meeting_id'], $user_id, $first_name, $last_name);
                if($response['code'] == 200) {
                    $join_url = $response['data']->join_url;
                }
            }
            else {
                $response = $this->service->addMeetingRegistrant($scheduled_meeting['zoom_meeting_id'], $user_id, $first_name, $last_name);
                if($response['code'] == 200) {
                    $join_url = $response['data']->join_url;
                }
            }

            return response()->json(['join_url' => $join_url], $response['code']);
        } catch(\Exception $e) {
            throw new \Exception("Error calling zoom API: " . $e->getMessage()); 
        }
    }

    public function callback()
    {
        $client = new Client(['base_uri' => 'https://zoom.us']);

        $response = $client->request('POST', '/oauth/token', [
            "headers"     => [
                "Authorization" => "Basic " . base64_encode(env("ZOOM_CLIENT_ID") . ':' . env("ZOOM_CLIENT_SECRET"))
            ],
            'form_params' => [
                "grant_type"   => "authorization_code",
                "code"         => $_GET['code'],
                "redirect_uri" => env("ZOOM_REDIRECT_URI")
            ],
        ]);

        $token = json_decode($response->getBody()->getContents(), true);

        Storage::disk('local')->put('zoom_oauth.txt', json_encode($token));
        echo "Access token inserted successfully.";
    }

    public function updateScheduledMeeting(Request $request, $meeting_id)
    {
        $this->validate($request, [
            'start_time' => 'int',
            'duration'   => 'int',
            'approval_type'   => 'int',
        ]);

        $scheduled_meetings = ZoomService::getScheduledMeetings();
        if (!isset($scheduled_meetings[$meeting_id]))
            return response()->json(['code' => 400, 'message' => 'Không tìm thấy buổi meeting', 'data' => []], 400);

        if ($scheduled_meetings[$meeting_id]['zoom_meeting_id'])
            return response()->json(['code' => 400, 'message' => 'Meeting đã được tạo, không được chỉnh sửa.', 'data' => []],
                                    400);

        if (isset($request->start_time) && $request->start_time < time())
            return response()->json(['code' => 400, 'message' => 'Bạn đang đặt lịch trong quá khứ. Không thể tạo lịch meeting.', 'data' => []],
                                    400);

        $new_start_time = $request->start_time ?? $scheduled_meetings[$meeting_id]['start_time'];
        $new_duration = $request->duration ?? $scheduled_meetings[$meeting_id]['duration'];
        $new_end_time = $new_start_time + $new_duration * 60;

        $hosts = ZoomService::getHosts();
        if (isset($request->start_time) && !ZoomService::canCreateScheduledMeeting($request->start_time, $new_end_time, [$meeting_id]))
            return response()->json(['code' => 400, 'message' => 'Bạn đã đạt đến số lượng (' . count($hosts) . ') cuộc họp tối đa diễn ra trong cùng một thời điểm', 'data' => []],
                                    400);

        $scheduled_meetings[$meeting_id]['start_time']      = $new_start_time;
        $scheduled_meetings[$meeting_id]['end_time']        = $new_end_time;
        $scheduled_meetings[$meeting_id]['duration']        = $new_duration;
        $scheduled_meetings[$meeting_id]['approval_type']   = $request->approval_type ?? $scheduled_meetings[$meeting_id]['approval_type'];

        ZoomService::setScheduledMeetings($scheduled_meetings);

        return response()->json(['code' => 200, 'message' => 'Success', 'data' => $scheduled_meetings[$meeting_id]]);
    }

    public function deleteScheduledMeeting($meeting_id)
    {
        $scheduled_meetings = ZoomService::getScheduledMeetings();

        if (isset($scheduled_meetings[$meeting_id])) {
            if ($scheduled_meetings[$meeting_id]['zoom_meeting_id']) {
                $response = ZoomService::getDetailMeeting($scheduled_meetings[$meeting_id]['zoom_meeting_id']);
                if ($response['code'] == 200) {
                    if ($response['data']['status'] == 'started')
                        return response()->json(['code' => 400, 'message' => 'Meeting đã được bắt đầu. Không được xóa.', 'data' => []], 400);
                    
                    ZoomService::deleteMeeting($scheduled_meetings[$meeting_id]['zoom_meeting_id']);
                }
            }

            unset($scheduled_meetings[$meeting_id]);
            ZoomService::setScheduledMeetings($scheduled_meetings);
        }

        return response()->json(['code' => 200, 'message' => 'Success', 'data' => []]);
    }

    public function createScheduledMeeting($topic, $start_time, $duration)
    {
        $end_time             = $start_time + $duration * 60;
        $scheduled_meeting_id = bin2hex(random_bytes(20));
        $hosts                = ZoomService::getHosts();
        // Log::info('Start create scheduled meeting', compact('topic', 'start_time', 'duration'));

        if ($start_time < time()) {
            // Log::info('Create scheduled meeting failure with message "Bạn đang đặt lịch trong quá khứ. Không thể tạo lịch meeting."', compact('topic', 'start_time', 'duration'));

            return response()->json([
                                        'code'    => 200,
                                        'message' => 'Bạn đang đặt lịch trong quá khứ. Không thể tạo lịch meeting.',
                                        'data'    => []
                                    ]);
                                }

        if (!ZoomService::canCreateScheduledMeeting($start_time, $end_time)) {
            // Log::info('Create scheduled meeting failure with message "Bạn đã đạt đến số lượng (' . count($hosts) . ') cuộc họp tối đa diễn ra trong cùng một thời điểm"', compact('topic', 'start_time', 'duration'));

            return response()->json([
                                        'code'    => 200,
                                        'message' => 'Bạn đã đạt đến số lượng (' . count($hosts) . ') cuộc họp tối đa diễn ra trong cùng một thời điểm',
                                        'data'    => []
                                    ]);
                                }

        $scheduled_meetings                        = ZoomService::getScheduledMeetings();
        $scheduled_meetings[$scheduled_meeting_id] = [
            'meeting_id'           => $scheduled_meeting_id,
            'topic'                => $topic,
            'start_time'           => $start_time,
            'end_time'             => $end_time,
            'duration'             => $duration,
            'host_password'        => bin2hex(random_bytes(20)),
            'participant_password' => bin2hex(random_bytes(20)),
            'zoom_meeting_id'      => '',
            'start_url'            => '',
            'join_url'              => '',
            'approval_type'        => env('ZOOM_DEFAULT_APPROVAL_TYPE'),
        ];

        if(ZoomService::setScheduledMeetings($scheduled_meetings)) {
            // Log::info('Create scheduled-meeting successfully', $scheduled_meetings);
            return response()->json(['code' => 200, 'message' => 'Success', 'data' => $scheduled_meetings[$scheduled_meeting_id]]);
        }

        // Log::info('Create scheduled-meeting failure', $scheduled_meetings);
        return response()->json(['code' => 200, 'message' => 'Có lỗi sảy ra khi tạo buổi meeting', 'data' => []]);
    }

    public function getScheduledMeetings() {
        return response()->json(ZoomService::getScheduledMeetings());
    }

    public function eventHandler(Request $request)
    {
        switch ($request->event) {
            case 'meeting.deleted':
            case 'meeting.ended':
                ZoomEventController::handleOnEndMeeting($request);
                break;
            case 'endpoint.url_validation':
                $message = 'v0:'.$request->header('x-zm-request-timestamp').':'.$request->getContent();
                $hash = hash_hmac('sha256', $message, env('ZOOM_WEBHOOK_SECRET'));
                $signature = "v0={$hash}";
                $verified = hash_equals($request->header('x-zm-signature'), $signature);
                if($verified)
                {
                    $zoomData = json_decode($request->getContent(), true);
                    $zoomSecret = env('ZOOM_WEBHOOK_SECRET');  
                    $zoomPlainToken = $zoomData['payload']['plainToken'];
                    $hash = hash_hmac('sha256', $zoomPlainToken, $zoomSecret);
                    $reponseData['plainToken'] = $zoomPlainToken;
                    $reponseData['encryptedToken'] = $hash;
                    return response()->json($reponseData);
                }
                break;
        }
    }

    public function resetToken()
    {
        ZoomService::resetToken();

        return response()->json((array)ZoomService::getToken());
    }

    public function resetHosts()
    {
        $response = $this->service->getUsers();

        if ($response['code'] == 200) {
            $hosts = [];
            foreach ($response['data']['users'] as $user) {
                // Chỉ lấy user type license và active user
                if ($user['type'] == 2 && $user['status'] == 'active')
                    $hosts[$user['id']] = $user['email'];
            }

            //$this->service->setAvailableHosts($hosts);
            $this->service->setHosts($hosts);

            exit('All hosts have been reset successfully!');
        }

        echo json_encode($response);
    }

    public function getAvailableHosts()
    {
        return response()->json(ZoomService::getAvailableHosts());
    }

    public function getHosts()
    {
        return response()->json(ZoomService::getHosts());
    }
}
