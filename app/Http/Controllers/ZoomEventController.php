<?php

namespace App\Http\Controllers;

use App\Services\ZoomService;
use Illuminate\Http\Request;

class ZoomEventController extends Controller
{
    public static function handleOnEndMeeting(Request $request)
    {
        $hosts          = ZoomService::getHosts();
        $availableHosts = ZoomService::getAvailableHosts();
        $hostId         = $request->payload['object']['host_id'];

        $availableHosts[$hostId] = $hosts[$hostId];

        ZoomService::setAvailableHosts($availableHosts);
    }
}