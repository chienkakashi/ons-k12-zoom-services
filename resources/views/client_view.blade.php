<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Live Class K12</title>

    <!-- For Client View -->
    <link type="text/css" rel="stylesheet" href="https://source.zoom.us/2.14.0/css/bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="https://source.zoom.us/2.14.0/css/react-select.css" />

    <link rel="stylesheet" href="{{ URL::asset('css/client_view.css', true) }}">

    <!-- Origin Trials to enable Gallery View in Chrome/Edge -->
    <!-- More Info: https://developers.zoom.us/docs/meeting-sdk/web/gallery-view/ -->
    <!-- SharedArrayBuffers in non-isolated pages on Desktop platforms -->
    <meta http-equiv="origin-trial" content="">
  </head>
  <body>

    <main>
      <div id="zmmtg-root">
        <!-- Zoom Meeting SDK Rendered Here -->
      </div>
    </main>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="URL::asset(\'js/vendor/jquery-1.9.1.min.js\', true)"><\/script>')
    </script>

    <!-- For Component and Client View -->
    <script src="https://source.zoom.us/2.14.0/lib/vendor/react.min.js"></script>
    <script src="https://source.zoom.us/2.14.0/lib/vendor/react-dom.min.js"></script>
    <script src="https://source.zoom.us/2.14.0/lib/vendor/redux.min.js"></script>
    <script src="https://source.zoom.us/2.14.0/lib/vendor/redux-thunk.min.js"></script>
    <script src="https://source.zoom.us/2.14.0/lib/vendor/lodash.min.js"></script>

    <!-- For Client View -->
    <script src="https://source.zoom.us/zoom-meeting-2.14.0.min.js"></script>
    <script>
        $(document).ready(function() {
          ZoomMtg.setZoomJSLib('https://source.zoom.us/2.14.0/lib', '/av')

          ZoomMtg.preLoadWasm()
          ZoomMtg.prepareWebSDK()
          // loads language files, also passes any error messages to the ui
          ZoomMtg.i18n.load('en-EN')
          ZoomMtg.i18n.reload('en-EN')

          $.ajax({
              url: "{{ env('APP_URL') }}/api/client_info/{{ $meeting_id }}/{{ $user_id }}/{{ $password }}/{{ $first_name }}/{{ $last_name }}",
              dataType: 'json',
              success: function(response) {
                console.log(response)
                if(response.code == 200) {
                  let signature = response.data.signature
                  let sdkKey = response.data.sdk_key
                  let meetingNumber = response.data.meeting_number
                  let passWord = response.data.password
                  let userName = response.data.username
                  let userEmail = response.data.user_email
                  let tk = response.data.registrant_token
                  let zak = response.data.zak_token
                  let leaveUrl = response.data.leave_url

                  startMeeting(signature, sdkKey, meetingNumber, passWord, userName, userEmail, tk, zak, leaveUrl);
                } else {
                  alert(response.message);
                  // open(location, '_self').close();
                  // history.back();
                }
              },
              error: (error) => {
                console.log(error)
              }
          });

          function startMeeting(signature, sdkKey, meetingNumber, passWord, userName, userEmail, tk, zak, leaveUrl) {

            document.getElementById('zmmtg-root').style.display = 'block'

            ZoomMtg.init({
              leaveUrl: leaveUrl,
              success: (success) => {
                console.log(success)
                ZoomMtg.join({
                  signature: signature,
                  sdkKey: sdkKey,
                  meetingNumber: meetingNumber,
                  passWord: passWord,
                  userName: userName,
                  userEmail: userEmail,
                  tk: tk,
                  zak: zak,
                  success: (success) => {
                    console.log(success)
                  },
                  error: (error) => {
                    console.log(error)
                  },
                })
              },
              error: (error) => {
                console.log(error)
              }
            })
          }
        })
    </script>

  </body>
</html>
