<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Zoom</title>

    <!--iOS -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="{{ URL::asset('css/normalize.css', true) }}">
    <link rel="stylesheet" href="{{ URL::asset('css/main.css', true) }}">
    <script src="{{ URL::asset('js/vendor/modernizr-2.6.2.min.js', true) }}"></script>
    <style type="text/css">
        .back-link a {
            color: #4ca340;
            text-decoration: none;
            border-bottom: 1px #4ca340 solid;
        }

        .back-link a:hover,
        .back-link a:focus {
            color: #408536;
            text-decoration: none;
            border-bottom: 1px #408536 solid;
        }

        h1 {
            height: 100%;
            /* The html and body elements cannot have any padding or margin. */
            margin: 0;
            font-size: 14px;
            font-family: 'Open Sans', sans-serif;
            font-size: 32px;
            margin-bottom: 3px;
        }

        .entry-header {
            text-align: left;
            margin: 0 auto 50px auto;
            width: 80%;
            max-width: 978px;
            position: relative;
            z-index: 10001;
        }

        #demo-content {
            padding-top: 100px;
        }
    </style>
</head>

<body>
    <!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->

    <!-- Demo content -->
    <div>
        <div id="loader-wrapper">
            <div id="loader"></div>

            <div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>

            <div class="loading-text">Đang thiết lập buổi học<span class="loading"></span></div>
        </div>

    </div>
    <!-- /Demo content -->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="URL::asset(\'js/vendor/jquery-1.9.1.min.js\', true)"><\/script>')
    </script>
    <script>
        $(document).ready(function() {
            setTimeout(function() {
                $('body').addClass('loaded');
                $('h1').css('color', '#222222');
            }, 99999999);

            $.ajax({
                url: "{{ env('APP_URL') }}/api/do-join/{{ $meeting_id }}/{{ $user_id }}/{{ $password }}/{{ $first_name }}/{{ $last_name }}",
                dataType: 'json',
                success: function(response) {
                    console.log(response);
                    if(response.code == 200) {
                        window.location.href = response.data.link;
                    } else {
                        alert(response.message);
                        // open(location, '_self').close();
                        history.back();
                    }
                }
            });
        });
    </script>

</body>

</html>