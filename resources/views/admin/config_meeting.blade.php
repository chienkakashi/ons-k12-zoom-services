<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Zoom Admin - Config Meeting</title>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="URL::asset(\'js/vendor/jquery-1.9.1.min.js\', true)"><\/script>')
    </script>

    <!--iOS -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="{{ URL::asset('css/normalize.css', true) }}">
    <link rel="stylesheet" href="{{ URL::asset('css/main.css', true) }}">
    <style type="text/css">
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            min-width: 100%;
        }

        td, th {
            border: 1px solid black;
            text-align: left;
            padding: 8px;
            min-width: 100%;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }

        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input { 
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #2196F3;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

            /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }

        .copied {
            width: 200px;
            opacity:0;
            position:fixed;
            /* bottom: 20px; */
            /* left: 0; */
            right: 0;
            margin: auto;
            color:#000;
            padding: 15px 15px;
            background-color: greenyellow;
            border: 1px solid greenyellow;
            transition: .4s opacity;
        }

        .actived {
            background-color: #2196F3;
        }
    </style>
</head>

<body>
    <div id="copied-success" class="copied">
        <span>Đã sao chép!</span>
    </div>
    <div style="display: flex; justify-content: center"><h2>Zoom Admin - Config Meeting</h2></div>
    <div>
        <table>
            <tr>
                <th>Meeting ID</th>
                <th>Tiêu Đề</th>
                <th>Thời Gian Bắt Đầu</th>
                <th>Thời Lượng (Phút)</th>
                <th>Start URL</th>
                <th>Join URL</th>
                <th>Đăng Ký Buổi Học</th>
            </tr>
            @foreach ($not_start_meetings as $meeting)
            <tr>
                <td>{{ $meeting['meeting_id'] }}</td>
                <td>{{ $meeting['topic'] }}</td>
                <td>{{ $meeting['start_time'] }}</td>
                <td>{{ $meeting['duration'] }}</td>
                <td>
                    @if($meeting['start_url'])
                        <div style="display: flex">
                            <div>{{ substr($meeting['start_url'], 0, 50) . '...' }}</div>
                            <span>
                                <button class="copy" data-text="{{$meeting['start_url']}}">
                                    <img style="max-width: 25px;" src="http://clipground.com/images/copy-4.png" title="Click to Copy">
                                </button>
                            </span>
                        </div>
                    @endif
                </td>
                <td>
                    @if($meeting['join_url'])
                    <div style="display: flex">
                        <div>{{ substr($meeting['join_url'], 0, 50) . '...' }}</div>
                        <span>
                            <button class="copy" data-text="{{$meeting['join_url']}}">
                                <img style="max-width: 25px;" src="http://clipground.com/images/copy-4.png" title="Click to Copy">
                            </button>
                        </span>
                    </div> 
                    @endif
                </td>
                <td>
                    <div style="display: flex">
                        <span>
                            <button 
                                id="registration-{{$meeting['meeting_id']}}"
                                class="registration @if($meeting['approval_type'] == 0 || $meeting['approval_type'] == 1) actived @endif"
                                data-meeting-id="{{$meeting['meeting_id']}}"
                                @if($meeting['start_url']) disabled @endif
                            >CÓ</button>
                        </span>
                        <span>
                            <button 
                                id="no-registration-{{$meeting['meeting_id']}}"
                                class="no-registration @if($meeting['approval_type'] == 2) actived @endif"
                                data-meeting-id="{{$meeting['meeting_id']}}"
                                @if($meeting['start_url']) disabled @endif
                            >KHÔNG</button>
                        </span>
                    </div>
                </td>
            </tr>
            @endforeach
        </table>
    </div>

    <script>
        $( document ).ready(function() {
            $('.copy').click(function (e) {
                e.preventDefault();

                let text = $(this).data('text');
                navigator.clipboard.writeText(text);

                $('#copied-success').css('opacity', 1);
                setTimeout(() => {
                    $('#copied-success').css('opacity', 0);
                }, 500);
            });

            $('.registration').click(function (e) {
                e.preventDefault();
                
                let actived = $(this).hasClass('actived')
                if(!actived) {
                    let meetingId = $(this).data('meeting-id');

                    $.ajax({
                        url: "{{ env('APP_URL') }}/api/scheduled-meeting/" + meetingId,
                        dataType: 'json',
                        type: 'PATCH',
                        data: {approval_type: 0, _method: "PATCH"},
                        success: function(response) {
                            console.log(response)
                            if(response.code == 200) {
                                $('#registration-' + meetingId).addClass('actived');
                                $('#no-registration-' + meetingId).removeClass('actived');
                            } else {
                                alert(response.message);
                            }
                        },
                        error: (error) => {
                            console.log(error)
                        }
                    });
                }
            })

            $('.no-registration').click(function (e) {
                e.preventDefault();
                
                let actived = $(this).hasClass('actived')
                if(!actived) {
                    let meetingId = $(this).data('meeting-id');

                    $.ajax({
                        url: "{{ env('APP_URL') }}/api/scheduled-meeting/" + meetingId,
                        dataType: 'json',
                        type: 'PATCH',
                        data: {approval_type: 2, _method: "PATCH"},
                        success: function(response) {
                            console.log(response)
                            if(response.code == 200) {
                                $('#no-registration-' + meetingId).addClass('actived');
                                $('#registration-' + meetingId).removeClass('actived');
                            } else {
                                alert(response.message);
                            }
                        },
                        error: (error) => {
                            console.log(error)
                        }
                    });
                }
            })
        });
    </script>
</body>

</html>