<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/join/{meeting_id}/{user_id}/{password}/{first_name}/{last_name}', 'ZoomController@join');

$router->get('/client_info/{meeting_id}/{user_id}/{password}/{first_name}/{last_name}', 'ZoomController@getClientInfo');

$router->get('/do-join/{meeting_id}/{user_id}/{password}/{first_name}/{last_name}', 'ZoomController@doJoinMeeting');

$router->get('/callback/index', 'ZoomController@callback');

$router->post('/event/handler', 'ZoomController@eventHandler');

$router->get('/reset-hosts', 'ZoomController@resetHosts');

$router->get('/available-hosts', 'ZoomController@getAvailableHosts');

$router->get('/hosts', 'ZoomController@getHosts');

$router->post('/reset-token', 'ZoomController@resetToken');

$router->get('/scheduled-meetings', 'ZoomController@getScheduledMeetings');

$router->patch('scheduled-meeting/{meeting_id}', 'ZoomController@updateScheduledMeeting');

$router->delete('scheduled-meeting/{meeting_id}', 'ZoomController@deleteScheduledMeeting');

$router->get('/create-scheduled-meeting/{topic}/{start_time}/{duration}', 'ZoomController@createScheduledMeeting');

// $router->get('/create_meeting/{topic}/{duration}', 'ZoomController@createMeeting');

$router->get('/login', function () {
    $url
        = "https://zoom.us/oauth/authorize?response_type=code&client_id=" . env("ZOOM_CLIENT_ID") . "&redirect_uri=" . env("ZOOM_REDIRECT_URI");

    echo "<a href='$url'>Login with Zoom</a>";
    exit;
});